$(function() {

    var isSafari = /^((?!chrome|android).)*safari/i.test(navigator.userAgent);
    if (isSafari) {
        $('html').addClass('_safari');
    };
});

var Main = Main || {};

$(function() {
    var modalLink = $('.js-modal-trigger');

    Main.openPopup = function (id) {
        var modal = $('.js-modal');

        if (!$('#'+id).length) return;

        //modal.removeClass('opened');
        //$('body').removeClass('overflow');

        $.each(modal, function() {
            if ($(this).attr('id') === id) {
                $(this).addClass('opened');
                $('body').addClass('overflow');
            }
        });

        return false;
    };

    Main.closePopup = function (id) {
        var modal = $('.js-modal');
        $.each(modal, function() {
            if ($(this).attr('id') === id) {
                $(this).removeClass('opened');
                $('body').removeClass('overflow');
            }
        });
    };

    modalLink.on('click', function(e) {
        var target = $(this).attr('data-target');

        e.preventDefault();
        Main.openPopup(target);
    });

    $('.js-close').on('click', function() {
        var target = $(this).data('target');

        Main.closePopup(target);
    });

    $(document).mouseup(function (e) {
        var modalW = $('.modal');
        if (modalW.has(e.target).length === 0 && !$('.js-modal.opened').is('._check-required')) {
            $('.js-modal').removeClass('opened');
            $('body').removeClass('overflow');
        }
    });

    $(this).keydown(function (eventObject) {
        if (eventObject.which == 27 && !$('.js-modal.opened').is('._check-required')) {
            $('body').removeClass('overflow');
            $('.js-modal').removeClass('opened');
        }
    });

    function openModalHash() {
        var hash = [],
            modal,
            i;

        $('.js-modal').each(function () {
            var id = $(this).attr('id');

            hash.push(id);
        });

        for (i = 0;i < hash.length; i++) {
            if ( '#'+hash[i] == window.location.hash && $('#'+hash[i]).length) {
                modal = hash[i];

                Main.openPopup(modal);
            }
        }
    }

    openModalHash();

    $('.js-search').on('focus', function() {
        $(this).next('.js-dropdown').addClass('show');
    });

    $('.js-search').on('blur', function() {
        $(this).next('.js-dropdown').removeClass('show');
    });

    $('.js-scroll-link').on("click", function (e) {
        var $target = $($(this).attr('href')),
            targetOffset = $target.offset().top;

        if ($target.length) {
            e.preventDefault();
            $('html, body').animate({
                scrollTop: targetOffset
            }, 600);
        }
    });

    var scrollingEl = $('.js-scroll-block');
    $.each(scrollingEl, function() {
        if ($(this).find('.js-scroll-inner').height() > $(this).height()) {
            $(scrollingEl).on('scroll', function() {
                if ($(this).find('.js-scroll-inner').outerHeight() - 40 <= $(this).scrollTop() + $(this).height()) {
                    $(this).closest('.js-inner-scroll').find('.js-onscroll').removeClass('disabled');
                }
            })
        } else {
            $(this).closest('.js-inner-scroll').find('.js-onscroll').removeClass('disabled');
        }
    });

    $(".js-play").on('click', function(){
        var symbol = $("#video")[0].src.indexOf("?") > -1 ? "&" : "?";
console.log('fs');
        $("#video")[0].src += symbol + "autoplay=1";
        $('.video-block__wrapper').addClass('play');
    });
});


